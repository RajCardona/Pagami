﻿namespace Pagami.ViewModels
{
    class MainViewModel
    {
        #region Propierties
        public  LoginViewModel  Login
        {
            get;
            set;
        }

        public AccountViewModel Accounts
        {
            get;
            set;
        }


        public InvoiceViewModel Invoices
        {
            get;
            set;
        }


        #endregion

        #region Constructors
        public  MainViewModel()
        {
            instance = this;
            Login = new LoginViewModel();
        }
        #endregion

        #region Sigleton
        static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }

            return instance;
        }
        #endregion
    }
}
