﻿namespace Pagami.ViewModels
{
    using Models;
    using System.Collections.ObjectModel;
    using Services;
    using System.ComponentModel;
    using System.Collections.Generic;
    using Newtonsoft.Json.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using Views;
    using Xamarin.Forms;

    public class AccountViewModel : INotifyPropertyChanged
    {
        #region Services
        ApiService apiService;
        DialogService dialogService;
        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Attributes
        ObservableCollection<Account> _accounts;
        ObservableCollection<User> _users;
        string _nameuser;
        string _accountbalance;
        string _numberaccount;
        string _typeaccount;

        string _idnumber;
        string _password;

      

        string[] PaymentData = new string[8];
        #endregion

        #region Properties
        public ObservableCollection<Account> Accounts
        {
            get
            {
                return _accounts; 
            }
            set
            {
                if (_accounts != value)
                {
                    _accounts = value;
                    PropertyChanged?.Invoke(
                        this,
                        new PropertyChangedEventArgs(nameof(Accounts)));
                }
            }
        }

        public ObservableCollection<User> Users
        {
            get
            {
                return _users;
            }
            set
            {
                if (_users != value)
                {
                    _users = value;
                    PropertyChanged?.Invoke(
                        this,
                        new PropertyChangedEventArgs(nameof(Users)));
                }
            }
        }

        public string  NameUser
        {
            get
            {
                return _nameuser;
            }
            set
            {
                if (_nameuser != value)
                {
                    _nameuser = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(NameUser)));
                }
            }
        }

        public string NumberAccount
        {
            get
            {
                return _numberaccount;
            }
            set
            {
                if (_numberaccount != value)
                {
                    _numberaccount = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(NumberAccount)));
                }
            }
        }

        public string TypeAccount
        {
            get
            {
                return _typeaccount;
            }
            set
            {
                if (_typeaccount != value)
                {
                    _typeaccount = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TypeAccount)));
                }
            }
        }

        public string AccountBalance
        {
            get
            {
                return _accountbalance;
            }
            set
            {
                if (_accountbalance != value)
                {
                    _accountbalance = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(AccountBalance)));
                }
            }
        }

        public List<Invoice> Invoices { get; set; }
        #endregion

        #region Constructor
        public AccountViewModel(string idnumber, string password)
        {
            _idnumber = idnumber;
            _password = password;

            apiService = new ApiService();
            dialogService = new DialogService();
            LoadAccounts(idnumber,password);
        }
        #endregion

        #region Methods
        async void LoadAccounts(string idnumber, string password)
        {
            var connection = await apiService.CheckConnection();

            if (!connection.IsSuccess)
            {
                await dialogService.ShowMessage(
                    "Error",
                    connection.Message);

                return;
            }

            var response = await apiService.GetAccountData<Account>(
                 "https://api.us.apiconnect.ibmcloud.com/playgroundbluemix-dev/hackathon/api/accounts",
               idnumber,
               password);

            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage(
                    "Error",
                    response.Message);
                return;
            }


            var accounts = (List<Account>)response.Result;

            string idquery = accounts[0].userId;

            response = await apiService.GetUserData(
                 "https://api.us.apiconnect.ibmcloud.com/playgroundbluemix-dev/hackathon/api/users/" + idquery);


            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage(
                    "Error",
                    response.Message);
                return;
            }

            var users = response.Result;

            var UserObj = JObject.Parse(response.Result.ToString());

            string username = UserObj["name"].ToString();

            NameUser = "Hola  " + username;

            NumberAccount = "Número de cuenta es: " + accounts[0].accountNumber;
            TypeAccount = "Tipo de cuenta: " + accounts[0].accountName;
            AccountBalance = "Su saldo es de: " + accounts[0].balance;


            PaymentData[0] = accounts[0].accountNumber; //sourceAccount
            PaymentData[1] = UserObj["documentType"].ToString();
            PaymentData[4] = "SAVING"; //targetTypeAccount
            PaymentData[5] = "SAVING"; //sourceTypeAccount 
            
          
        }

        #endregion

        #region Commands
        public ICommand InvoiceCommand
        {
            get
            {
                return new RelayCommand(InvoCommand);
            }
        }

       

        
        

        async void InvoCommand()
        {
            var connection = await apiService.CheckConnection();

            if (!connection.IsSuccess)
            {
                await dialogService.ShowMessage(
                    "Error",
                    connection.Message);

                return;
            }

            try
            {
                var scanner = DependencyService.Get<IQrScanningService>();
                var result = await scanner.ScanAsync();
                if (result != null)
                {
                    var mainViewModel = MainViewModel.GetInstance();
                    mainViewModel.Invoices = new InvoiceViewModel(PaymentData, result, _idnumber, _password);
                    await Application.Current.MainPage.Navigation.PushAsync(new InvoiceView());
                }
            }
            catch (System.Exception)
            {

                throw;
            }


            

        }


        #endregion
    }

}

