﻿namespace Pagami.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using System.ComponentModel;
    using System.Windows.Input;
    using Services;
    using System.Collections.ObjectModel;
    using Models;
    using Xamarin.Forms;
    using Views;

    class LoginViewModel : INotifyPropertyChanged
    {
        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Services
        ApiService apiService;
        DialogService dialogService;
        #endregion

        #region Attributes
        string _idnumber;
        string _password;
        bool _isToggled;
        bool _isRunning;
        bool _isEnabled;
        ObservableCollection<Account> _accounts;

       
        #endregion

        #region Properties

        public ObservableCollection<Account> Accounts
        {
            get
            {
                return _accounts;
            }
            set
            {
                if (_accounts != value)
                {
                    _accounts = value;
                    PropertyChanged?.Invoke(
                        this,
                        new PropertyChangedEventArgs(nameof(Account)));
                }
            }
        }
        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                if (_isEnabled != value)
                {
                    _isEnabled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsEnabled)));
                }
            }
        }

        public bool IsRunning
        {
            get
            {
                return _isRunning;
            }
            set
            {
                if (_isRunning != value)
                {
                    _isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsRunning)));
                }
            }
        }

        public bool IsToggled
        {
            get
            {
                return _isToggled;
            }
            set
            {
                if (_isToggled != value)
                {
                    _isToggled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsToggled)));
                }
            }
        }

        public string Idnumber
        {
            get
            {
                return _idnumber;
            }
            set
            {
                if (_idnumber != value)
                {
                    _idnumber = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Idnumber)));
                }
            }
        }

        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                if (_password != value)
                {
                    _password = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Password)));
                }
            }
        }
        #endregion

        #region Constructors
        public LoginViewModel()
        {
            apiService = new ApiService();
            dialogService = new DialogService();
            IsEnabled = true;
            IsToggled = true;   
        }
        #endregion

        #region Commands
        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
         
        }

        async void Login()
        {
            if (string.IsNullOrEmpty(Idnumber))
            {
                await dialogService.ShowMessage(
                    "Error",
                    "Debe ingresar un  usuario");
                return;
            }

            if (string.IsNullOrEmpty(Password))
            {
                await dialogService.ShowMessage(
                    "Error",
                    "Debe Ingresar una Contraseña");
                return;
            }

            IsRunning = true;
            IsEnabled = false;

            var connection = await apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                IsRunning = false;
                IsEnabled = true;
                await dialogService.ShowMessage("Error", connection.Message);
            }




            var response = await apiService.ValidateExistanceAccount(
               "https://api.us.apiconnect.ibmcloud.com/playgroundbluemix-dev/hackathon/api/accounts",
               Idnumber,
               Password);

            if (response == null)
            {
                IsRunning = false;
                IsEnabled = true;
                await dialogService.ShowMessage(
                    "Error",
                    "El servicio no está disponible intente más tarde");
                Password = null;
                return;
            }
            else if(response.IsSuccess == false)
            {
                IsRunning = false;
                IsEnabled = true;
                await dialogService.ShowMessage(
                    "Error",
                    "Credenciales Incorrectas");
                Password = null;
                return;
            }


            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.Accounts = new AccountViewModel(Idnumber.ToString(),Password.ToString());
            await Application.Current.MainPage.Navigation.PushAsync(new AccountView());

            Idnumber = null;
            Password = null;

            IsRunning = false;
            IsEnabled = true;
        

        }
        #endregion
    }
}

 