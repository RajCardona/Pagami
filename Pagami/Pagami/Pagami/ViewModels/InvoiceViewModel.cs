﻿namespace Pagami.ViewModels
{
    using Models;
    using System.Collections.ObjectModel;
    using Services;
    using System.ComponentModel;
    using System.Collections.Generic;
    using Newtonsoft.Json.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using Views;
    using Xamarin.Forms;

    public class InvoiceViewModel : INotifyPropertyChanged
    {
        #region Services
        ApiService apiService;
        DialogService dialogService;
        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Attributes
        ObservableCollection<Account> _accounts;
        ObservableCollection<User> _users;
        string _invoicenumber;
        string _invoicemerchan;
        string _invoicedescription;
        string _invoiceamount;

        string _accountbalance;
        string _numberaccount;
        string _typeaccount;

        string _idnumber;
        string _password;

        string[] arg= new string[8];

        #endregion

        #region Properties


        public ObservableCollection<Account> Accounts
        {
            get
            {
                return _accounts; 
            }
            set
            {
                if (_accounts != value)
                {
                    _accounts = value;
                    PropertyChanged?.Invoke(
                        this,
                        new PropertyChangedEventArgs(nameof(Accounts)));
                }
            }
        }

        public ObservableCollection<User> Users
        {
            get
            {
                return _users;
            }
            set
            {
                if (_users != value)
                {
                    _users = value;
                    PropertyChanged?.Invoke(
                        this,
                        new PropertyChangedEventArgs(nameof(Users)));
                }
            }
        }

       

        public string InvoiceNumber
        {
            get
            {
                return _invoicenumber;
            }
            set
            {
                if (_invoicenumber != value)
                {
                    _invoicenumber = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(InvoiceNumber)));
                }
            }
        }

        public string InvoiceMerchan
        {
            get
            {
                return _invoicemerchan;
            }
            set
            {
                if (_invoicemerchan != value)
                {
                    _invoicemerchan = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(InvoiceMerchan)));
                }
            }
        }

        public string InvoiceDescription
        {
            get
            {
                return _invoicedescription;
            }
            set
            {
                if (_invoicedescription != value)
                {
                    _invoicedescription = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(InvoiceDescription)));
                }
            }
        }

        public string InvoiceAmount
        {
            get
            {
                return _invoiceamount;
            }
            set
            {
                if (_invoiceamount != value)
                {
                    _invoiceamount = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(InvoiceAmount)));
                }
            }
        }

        public List<Invoice> Invoices { get; set; }
        string[] PaymentData = new string[8];
        #endregion

        #region Constructor
        public InvoiceViewModel(string[] payData, string idfact, string idnumber, string password)
        {
            _idnumber = idnumber;
            _password = password;

            apiService = new ApiService();
            dialogService = new DialogService();
            LoadAccounts(idfact, idnumber,password);

            
            PaymentData = payData;


        }
        #endregion

        #region Methods
        async void LoadAccounts(string idfact, string idnumber, string password)
        {
            var connection = await apiService.CheckConnection();

            if (!connection.IsSuccess)
            {
                await dialogService.ShowMessage(
                    "Error",
                    connection.Message);

                return;
            }

            


            

            var response = await apiService.GetInvoiceData<Invoice>(
                 "https://api.us.apiconnect.ibmcloud.com/playgroundbluemix-dev/hackathon/api/invoices?filter[where][invoiceNumber]=" + idfact,
               idnumber,
               password);

            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage(
                    "Error",
                    response.Message);
                return;
            }

            var invoices = (List<Invoice>)response.Result;

            InvoiceNumber = "Número de la factura:  " + invoices[0].invoiceNumber;
            InvoiceMerchan = "Nombre del Comercio: " + invoices[0].merchantName;
            InvoiceDescription = "Descripción: " + invoices[0].description;
            InvoiceAmount = "Valor de la Factura: $" + invoices[0].amount+""+invoices[0].currency;

            PaymentData[2] = invoices[0].amount.ToString();
            PaymentData[3] = invoices[0].description;
            PaymentData[6] = invoices[0].currency;
            PaymentData[7] = invoices[0].id;

         

        }

        async void MakePayment(string idnumber, string password)
        {
            var connection = await apiService.CheckConnection();

            if (!connection.IsSuccess)
            {
                await dialogService.ShowMessage(
                    "Error",
                    connection.Message);

                return;
            }



            var payment = new Payment
            {
                sourceAccount = PaymentData[0],
                targetTypeDocument = PaymentData[1],
                amountPayment = PaymentData[2],
                description = PaymentData[3],
                targetTypeAccount = PaymentData[4],
                sourceTypeAccount = PaymentData[5],
                currency = PaymentData[6],
                invoiceId = PaymentData[7],

                //sourceAccount = "16636324199",
                //targetTypeDocument = "CC",
                //amountPayment = "70000",
                //description = "Pago factura compraventa el rodro",
                //targetTypeAccount = "SAVING",
                //sourceTypeAccount = "SAVING",
                //currency = "COP",
                //invoiceId = "84788407bd9a662aa7d6d99964cb35af",
            };


            var response = await apiService.PostPaymentInvoice<Payment>(
                 "https://api.us.apiconnect.ibmcloud.com/playgroundbluemix-dev/hackathon/api/payments",
               idnumber,
               password,
               payment);

            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage(
                    "Error",
                    response.Message);
                return;
            }

            


            await dialogService.ShowMessage(
                   "Exitoso",
                   "Se realizó el pago correctamente haz ganado 3 puntos ecológicos");

            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.Accounts = new AccountViewModel(_idnumber, _password);
            await Application.Current.MainPage.Navigation.PushAsync(new AccountView());
        }
        #endregion

        #region Commands
        public ICommand InvoiceCommand
        {
            get
            {
                return new RelayCommand(InvoCommand);
            }
        }

        async void InvoCommand()
        {




            MakePayment(_idnumber,_password);



        }
        #endregion
    }

}

