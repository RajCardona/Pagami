﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pagami.Models
{
    public class Payment
    {

        [JsonProperty(PropertyName = "sourceAccount")]
        public string sourceAccount { get; set; }
        [JsonProperty(PropertyName = "targetTypeDocument")]
        public string targetTypeDocument { get; set; }
        [JsonProperty(PropertyName = "amountPayment")]
        public string amountPayment { get; set; }
        [JsonProperty(PropertyName = "description")]
        public string description { get; set; }
        [JsonProperty(PropertyName = "targetTypeAccount")]
        public string targetTypeAccount { get; set; }
        [JsonProperty(PropertyName = "sourceTypeAccount")]
        public string sourceTypeAccount { get; set; }
        [JsonProperty(PropertyName = "currency")]
        public string currency { get; set; }
        [JsonProperty(PropertyName = "invoiceId")]
        public string invoiceId { get; set; }
      
  
    }
}
