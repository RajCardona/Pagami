﻿namespace Pagami.Models
{
    using System;

    public class User
    {
        public string documentId { get; set; }
        public string documentType { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public DateTime creationDate { get; set; }
        public string id { get; set; }
    }
}
