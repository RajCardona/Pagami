﻿namespace Pagami.Models
{
    using System;

    public class Account
    {
        public int balance { get; set; }
        public string accountName { get; set; }
        public string accountType { get; set; }
        public string id { get; set; }
        public DateTime creationDate { get; set; }
        public string accountNumber { get; set; }
        public string userId { get; set; }
    }
}
