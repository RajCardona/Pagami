﻿using Xamarin.Forms;

[assembly: Dependency(typeof(Pagami.Droid.Services.QrScanningService))]

namespace Pagami.Droid.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Android.App;
    using Android.Content;
    using Android.OS;
    using Android.Runtime;
    using Android.Views;
    using Android.Widget;
    using Pagami.Services;
    using ZXing.Mobile;
    using System.Threading.Tasks;
    
   

    public class QrScanningService : IQrScanningService
    {
        public async Task<string> ScanAsync() 
        {
            var optionsDefault = new MobileBarcodeScanningOptions();
            var optionsCustom = new MobileBarcodeScanningOptions();

            var scanner = new MobileBarcodeScanner()
            {
                TopText = "Acerca la camara al elemento",
                BottomText = "Toca la pantalla para enfocar",
            };

            var scanResult = await scanner.Scan(optionsCustom);
            return scanResult.Text;
        }
    }
}